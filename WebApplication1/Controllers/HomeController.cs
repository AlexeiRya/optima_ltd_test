﻿using Newtonsoft.Json;
using Ninject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models.Repository.Employees;

namespace WebApplication1.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly IEmployeRepository _employeRepository;
        public HomeController()
        {
            IKernel ninjectKernel = new StandardKernel();
            ninjectKernel.Bind<IEmployeRepository>().To<EmployeRepository>();
            _employeRepository = ninjectKernel.Get<IEmployeRepository>();
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetStatuses()
        {
            try
            {
                var data = _employeRepository.GetStatuses();
                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject(data),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject(ex.Message),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
        [HttpGet]
        public JsonResult GetPositions()
        {
            try
            {
                var data = _employeRepository.GetPositions();
                string result = JsonConvert.SerializeObject(data);
                return new JsonResult
                {
                    Data = result,
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject(ex.Message),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }

        [HttpGet]
        public JsonResult GetSalary()
        {
            try
            {
                var data = _employeRepository.GetSalary();
                string result = JsonConvert.SerializeObject(data);
                return new JsonResult
                {
                    Data = result,
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject(ex.Message),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
        [HttpGet]
        public JsonResult GetEmployees()
        {
            try
            {
                var data = _employeRepository.GetEmployees();
                string result = JsonConvert.SerializeObject(data);
                return new JsonResult
                {
                    Data = result,
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject(ex.Message),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }

        [HttpPost]
        public JsonResult AddNewEmploye(Employees inputModel)
        {
            try
            {
                _employeRepository.AddEmploye(inputModel);
                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject("success"),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject(ex.Message),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }

        [HttpPost]
        public JsonResult DismissEmployee(int? EmployeId)
        {
            try
            {
                if (EmployeId != null)
                    _employeRepository.DismissEmployee(EmployeId);
                else
                    throw new Exception("id = null");
                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject("success"),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject(ex.Message),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }


        [HttpPost]
        public JsonResult EmployeSearch(string AutocompleteSearchString)
        {
            try
            {
                var data = _employeRepository.EmployeSearch(AutocompleteSearchString);

                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject(data),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = JsonConvert.SerializeObject(ex.Message),
                    ContentType = "application/json",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
    }
}