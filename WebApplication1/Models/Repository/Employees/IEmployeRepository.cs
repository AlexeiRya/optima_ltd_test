﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Models.Repository.Employees
{
    interface IEmployeRepository
    {
        List<WebApplication1.Models.Repository.Employees.EmployeStatus> GetStatuses();
        List<WebApplication1.Models.Repository.Employees.EmployePosition> GetPositions();
        List<WebApplication1.Models.Repository.Employees.EmployeSalary> GetSalary();
        List<WebApplication1.Models.Repository.Employees.Employees> GetEmployees();


        void AddEmploye(Employees inputModel);
        void DismissEmployee(int? inputModel);
        List<string> EmployeSearch(string AutocompleteSearchString);




    }
}
