﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.Repository.Employees
{
    public class EmployePosition
    {
        public decimal EmployePositionId { get; set; }
        public string EmployePositionName { get; set; }
        public decimal EmployeSalaryId { get; set; }
    }
    public class EmployeSalary
    {
        public decimal EmployeSalaryId { get; set; }
        public decimal EmployeSalaryValue { get; set; }
    }
    public class EmployeStatus
    {
        public decimal EmployeStatusId { get; set; }
        public string EmployeStatusName { get; set; }
    }
    public class Employees
    {
        public decimal EmployeId { get; set; }
        public string EmployeName { get; set; }
        public string EmployeSurname { get; set; }
        public decimal EmployeStatusId { get; set; }
        public decimal EmployePositionId { get; set; }
    }
    public class EmployeRepository : IEmployeRepository
    {
        public List<WebApplication1.Models.Repository.Employees.EmployePosition> GetPositions()
        {
            using(var _context = new EmployeesTestDBEntities())  
                return _context.EmployePositions.Select(x=> new EmployePosition() {
                    EmployePositionId = x.EmployePositionId,
                    EmployePositionName = x.EmployePositionName,
                    EmployeSalaryId = x.EmployeSalaryId
                }).ToList();    
        }

        public List<WebApplication1.Models.Repository.Employees.EmployeStatus> GetStatuses()
        {
            using (var _context = new EmployeesTestDBEntities())
                return _context.EmployeStatus.Select(x => new EmployeStatus()
                {
                    EmployeStatusId = x.EmployeStatusId,
                    EmployeStatusName = x.EmployeStatusName
                }).ToList();  
        }
        public List<WebApplication1.Models.Repository.Employees.EmployeSalary> GetSalary()
        {
            using (var _context = new EmployeesTestDBEntities())
                return _context.EmployeSalaries.Select(x => new EmployeSalary()
                {
                    EmployeSalaryId = x.EmployeSalaryId,
                    EmployeSalaryValue = x.EmployeSalaryValue
                }).ToList();
        }
        public List<WebApplication1.Models.Repository.Employees.Employees> GetEmployees()
        {
            using (var _context = new EmployeesTestDBEntities())
                return _context.Employees.Select(x => new Employees()
                {
                    EmployeId = x.EmployeId,
                    EmployeName = x.EmployeName,
                    EmployeSurname = x.EmployeSurname,
                    EmployePositionId = x.EmployePositionId,
                    EmployeStatusId = x.EmployeStatusId
                }).ToList();
        }



        public void AddEmploye(Employees inputModel)
        {
            using(var _context = new EmployeesTestDBEntities())
            {

                Employee employee = new Employee() {
                    EmployeId = inputModel.EmployeId,
                    EmployeName = inputModel.EmployeName,
                    EmployeSurname = inputModel.EmployeSurname,
                    EmployePositionId = inputModel.EmployePositionId,
                    EmployeStatusId = inputModel.EmployeStatusId
                };
                _context.Employees.Add(employee);
                _context.SaveChanges();
            }
        }

        public void DismissEmployee(int? inputModel)
        {
            using (var _context = new EmployeesTestDBEntities())
            {

                var employe = _context.Employees.FirstOrDefault(x => x.EmployeId == inputModel);
                if(employe != null)
                {
                    employe.EmployeStatusId = 3;
                    _context.SaveChanges();
                }
            }
        }
        List<string> IEmployeRepository.EmployeSearch(string AutocompleteSearchString)
        {
            List<string> outputModel = new List<string>();
            using (var _context = new EmployeesTestDBEntities())
            {
                var items = _context.Employees.Where(x => x.EmployeName.Contains(AutocompleteSearchString)).OrderBy(x=>x.EmployeId).Take(10).ToList();
                foreach(var item in items)
                {
                    outputModel.Add(item.EmployeName);
                }
            }
            return outputModel;
        }
    }
}