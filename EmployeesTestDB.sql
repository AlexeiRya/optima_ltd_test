
--CREATE DATABASE EmployeesTestDB COLLATE Cyrillic_General_CI_AS;
--use EmployeesTestDB
--go
--drop table Employees;
--drop table EmployePosition;
--drop table EmployeSalary;
--drop table EmployeStatus;

use EmployeesTestDB
go

CREATE TABLE EmployeStatus
(
    EmployeStatusId numeric(1, 0) PRIMARY KEY IDENTITY(1, 1) not null,
	EmployeStatusName nvarchar(50) not null,
	EmployeStatusDescription nvarchar(500)
);
CREATE TABLE EmployeSalary
(
    EmployeSalaryId numeric(1, 0) PRIMARY KEY IDENTITY(1, 1) not null,
	EmployeSalaryValue numeric(8, 0) not null
);
CREATE TABLE EmployePosition
(
    EmployePositionId numeric(2, 0) PRIMARY KEY IDENTITY(1, 1) not null,
	EmployePositionName nvarchar(50) not null,
	EmployeSalaryId numeric(1, 0) not null,
	EmployePositionDescription nvarchar(500) not null,
	CONSTRAINT FK_EmployePosition_To_EmployeSalary FOREIGN KEY (EmployeSalaryId)  REFERENCES EmployeSalary (EmployeSalaryId)

);
CREATE TABLE Employees
(
    EmployeId numeric(8, 0) PRIMARY KEY IDENTITY(1, 1) not null,
    EmployeName nvarchar(200) not null,
	EmployeSurname nvarchar(200) not null,
	EmployeStatusId numeric(1, 0) not null,
	EmployePositionId numeric(2, 0) not null,
	CONSTRAINT FK_Employees_To_EmployeStatus FOREIGN KEY (EmployeStatusId)  REFERENCES EmployeStatus (EmployeStatusId),
	CONSTRAINT FK_Employees_To_EmployePosition FOREIGN KEY (EmployePositionId)  REFERENCES EmployePosition (EmployePositionId)
);



insert into EmployeStatus (EmployeStatusName, EmployeStatusDescription)
values
('Работает', ''),
('Не работает', ''),
('Уволен', '');

insert into EmployeSalary (EmployeSalaryValue)
values
(100000),
(200000),
(300000);


insert into EmployePosition (EmployePositionName, EmployeSalaryId, EmployePositionDescription)
values
('Директор', 3, ''),
('Раб', 3, ''),
('Надзиратель', 3, '');


insert into Employees (EmployeName, EmployeSurname, EmployeStatusId, EmployePositionId)
values
('александр', 'лукашенко', 1, 1),
('василий', 'пупкин', 2, 2),
('иван', 'иванов', 3, 3);
